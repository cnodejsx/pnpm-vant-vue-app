import {createRouter, createWebHashHistory  } from 'vue-router'

const routes = [
  {
    name: 'notFound',
    path: '/:path(.*)+',
    redirect: {
      name: 'goods',
    },
  },
  {
    name: 'goods',
    path: '/goods',
    component: () => import('../views/Goods/index.vue'),
    meta: {
      title: '商品详情',
    },
  },
  {
    name: 'about',
    path: '/about',
    component: () => import('../views/About/index.vue'),
    meta: {
      title: '关于',
    },
  },
]

const router = createRouter({
  routes,
  history: createWebHashHistory(),
});

export default router